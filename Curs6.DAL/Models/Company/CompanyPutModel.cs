﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Curs6.DAL.Models.Company
{
    public class CompanyPutModel
    {
        public string Name { get; set; }
        public string WebSite { get; set; }
    }
}
