﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Curs6.DAL.Models.Company
{
    public class CompanyPostModel
    {
        public string Name { get; set; }
        public string Website { get; set; }
        public string ContactEmail { get; set; }
    }
}
