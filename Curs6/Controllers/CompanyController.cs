﻿using Curs6.DAL.Models.Company;
using Curs6.Services.CompanyServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Curs6.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private readonly ICompanyService _companyService;

        public CompanyController(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        [HttpPost("add-company")]
        public async Task<ActionResult> AddCompany([FromBody]CompanyPostModel model)
        {
            await _companyService.AddCompany(model);


            return Ok();
        }

        [HttpPut()]
        public async Task<ActionResult> UpdateCompany([FromBody] CompanyPutModel model, int id)
        {
            await _companyService.UpdateCompany(model, id);

            return Ok();
        }
    }
}
