﻿using Curs6.DAL.Models.Company;

namespace Curs6.Services.CompanyServices
{
    public interface ICompanyService
    {
        Task AddCompany(CompanyPostModel model);
        Task UpdateCompany(CompanyPutModel model, int id);
    }
}