﻿using Curs6.DAL;
using Curs6.DAL.Entities;
using Curs6.DAL.Models.Company;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Curs6.Services.CompanyServices
{
    public class CompanyService : ICompanyService
    {
        private readonly AppDbContext _context;
        public CompanyService(AppDbContext context)
        {
            _context = context;
        }

        public async Task AddCompany(CompanyPostModel model)
        {
            var company = new Company()
            {
                ContactEmail = model.ContactEmail,
                Name = model.Name,
                Website = model.Website
            };

            
            await _context.AddAsync(company);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateCompany(CompanyPutModel model, int id)
        {
            try
            {
                var company = await _context.Companies.FirstOrDefaultAsync(x => x.Id == id);
           
            //var company = await _context.Companies.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            
            if (company == null)
            {
                return ;
            }

            company.Name = model.Name;
            company.Website = model.WebSite;

            
            await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }
        }
    }
}
