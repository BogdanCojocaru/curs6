﻿using Curs6.Services.CompanyServices;
using Curs6.Services.EmployeeProjectsServices;
using Curs6.Services.EmployeeServices;
using Curs6.Services.ProjectServices;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Curs6.Services
{
    public static class ServiceExtension
    {
        public static void AddServices(this IServiceCollection services)
        {
            services.AddTransient<IEmployeeService, EmployeeService>();
            services.AddTransient<ICompanyService, CompanyService>();
            services.AddTransient<IProjectService, ProjectService>();
            services.AddTransient<IEmployeeProjectService, EmployeeProjectService>();

        }
    }
}
